import { Component, OnInit } from '@angular/core';
import {Dodawanie} from '../function/fun';
import {ReflectiveInjector} from '@angular/core';
import { FormControl, FormGroup, FormBuilder, AbstractControl, Validators } from '@angular/forms';
export var titles: Array<any> = [
    { "tytul": "Wiedzmin 3",  "gatunek": "RPG"},
    { "tytul": "Mass Effect:  Andromeda",  "gatunek": "RPG"},
    { "tytul": "Battlefield 3",  "gatunek": "FPS" },
    { "tytul": "Alan Wake",  "gatunek": "Przygodowa, Akcja"},
    { "tytul": "The Wolf Amoung Us",  "gatunek": "Przygodowa", "ocena": "8", "studio": "Telltale Games" }
];

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {
   titles: Array<any>;
  category: string[];
  myForm: FormGroup;

  tytul: AbstractControl;
  kategoria: AbstractControl;
  multipliedValue: any;
  dodawacz: Dodawanie;

  constructor(fb: FormBuilder) {
    this.titles=titles;
    const injector: any = ReflectiveInjector.resolveAndCreate([Dodawanie]);
    this.dodawacz = injector.get(Dodawanie);
   
    this.myForm = fb.group({
      "tytul": ['', Validators.required],
      "kategoria": ['', Validators.required]
    });
        this.tytul = this.myForm.controls['tytul'];
        this.kategoria = this.myForm.controls['kategoria'];
   }

  ngOnInit() {
  }
    
  

add(value: any) {

    this.titles = this.dodawacz.dodaj(value.tytul, value.kategoria, this.titles);


  }
}
