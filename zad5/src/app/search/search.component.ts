import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import {Szukaj} from '../function/fun2';
import {ReflectiveInjector} from '@angular/core';
import { titles } from '../game/game.component';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  searchControl: FormControl;
  searches: Array<any>;
  su: Szukaj;
  constructor(  ) { 
    const injector: any = ReflectiveInjector.resolveAndCreate([Szukaj]);
    this.su = injector.get(Szukaj);
    this.searchControl = new FormControl();

    this.searchControl.valueChanges.subscribe(
     
     
     
     searchItem => this.su.wypisz(this.searchControl, titles)

     
    );
  }

  ngOnInit() {
  }

}
