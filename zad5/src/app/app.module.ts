import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { GryItemComponent } from './gry-item/gry-item.component';
import { GameComponent } from './game/game.component';
import { SearchComponent } from './search/search.component';

@NgModule({
  declarations: [
    AppComponent,

    GryItemComponent,
    GameComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule
  ],
  providers: [  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
