import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { GryItemComponent } from './gry-item/gry-item.component';
import { GameComponent } from './game/game.component';
import { SearchComponent } from './search/search.component';
import { RouterModule, Routes} from '@angular/router';
import { ConfirmContactGuard} from './function/confirm';
import { GameDeleteComponent } from './game-delete/game-delete.component';
import { GameEdycjaComponent } from './game-edycja/game-edycja.component';

const routes: Routes = [
  // basic routes
  { path: '', redirectTo: '', pathMatch: 'full' },
  { path: 'dodaj', component: GameComponent },
  { path: 'usun', component: GameDeleteComponent },
  { path: 'szukaj', component: SearchComponent,  canDeactivate: [ConfirmContactGuard], },
  { path: 'edytuj', component: GameEdycjaComponent },



];


@NgModule({
  declarations: [
    AppComponent,

    GryItemComponent,
    GameComponent,
    SearchComponent,
    GameDeleteComponent,
    GameEdycjaComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forRoot(routes)
  ],
  providers: [ 
    ConfirmContactGuard
   ],
  bootstrap: [AppComponent]
})
export class AppModule { }
