import { AppComponent } from './../app.component';
import { Component, OnInit } from '@angular/core';
import {Dodawanie} from '../function/fun';
import {ReflectiveInjector} from '@angular/core';
import { FormControl, FormGroup, FormBuilder, AbstractControl, Validators } from '@angular/forms';
import { titles } from  './../app.component';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {
  titles: Array<any>;
  category: string[];
  myForm: FormGroup;

  tytul: AbstractControl;
  kategoria: AbstractControl;
  dodawacz: Dodawanie;

  constructor(fb: FormBuilder) {
    this.titles=titles;
    const injector: any = ReflectiveInjector.resolveAndCreate([Dodawanie]);
    this.dodawacz = injector.get(Dodawanie);
   
    this.myForm = fb.group({
      "tytul": ['', Validators.required],
      "kategoria": ['', Validators.required]
    });
        this.tytul = this.myForm.controls['tytul'];
        this.kategoria = this.myForm.controls['kategoria'];
   }

  ngOnInit() {
  }
    
  

add(value: any) {
    this.titles = this.dodawacz.dodaj(value.tytul, value.kategoria, this.titles);
  }
}
