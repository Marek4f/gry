import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GryItemComponent } from './gry-item.component';

describe('GryItemComponent', () => {
  let component: GryItemComponent;
  let fixture: ComponentFixture<GryItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GryItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GryItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
