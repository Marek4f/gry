import { Component, OnInit, Input,Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-gry-item',
  templateUrl: './gry-item.component.html',
  styleUrls: ['./gry-item.component.css']
})
export class GryItemComponent implements OnInit {

@Input()
name: string;
@Input()
t: boolean;
@Output()
clickEmitter: EventEmitter<string> = new EventEmitter();
  constructor() {
    this.t=true;
   }

  ngOnInit() {
    
  }
  gameSelected() {
    this.clickEmitter.emit(this.name);
    this.t = !this.t;


  }

}
