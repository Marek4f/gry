import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameEdycjaComponent } from './game-edycja.component';

describe('GameEdycjaComponent', () => {
  let component: GameEdycjaComponent;
  let fixture: ComponentFixture<GameEdycjaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameEdycjaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameEdycjaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
