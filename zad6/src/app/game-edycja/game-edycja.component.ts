import { AppComponent } from './../app.component';
import { Component, OnInit } from '@angular/core';
import { titles } from  './../app.component';
import { FormControl, FormGroup, FormBuilder, AbstractControl, Validators } from '@angular/forms';
import {Edytowanie} from '../function/fun4';
import {ReflectiveInjector} from '@angular/core';

@Component({
  selector: 'app-game-edycja',
  templateUrl: './game-edycja.component.html',
  styleUrls: ['./game-edycja.component.css']
})
export class GameEdycjaComponent implements OnInit {
  titles: Array<any>;
  category: string[];
  myForm3: FormGroup;

  tytul: AbstractControl;
  ntytul: AbstractControl;
  nkategoria: AbstractControl;
  edytowacz: Edytowanie;

  constructor(fb: FormBuilder) { 
    this.titles=titles;
    const injector: any = ReflectiveInjector.resolveAndCreate([Edytowanie]);
    this.edytowacz = injector.get(Edytowanie);
   
    this.myForm3= fb.group({
      "tytul": ['', Validators.required],
      "ntytul": ['', Validators.required],
      "nkategoria": ['', Validators.required]

    });
        this.tytul = this.myForm3.controls['tytul'];
        this.ntytul = this.myForm3.controls['ntytul'];
        this.nkategoria = this.myForm3.controls['nkategoria'];
  }

  ngOnInit() {
  }

  edit(value: any) {
    this.titles = this.edytowacz.edytuj(value.tytul, value.ntytul, value.nkategoria, this.titles);
  }

}
