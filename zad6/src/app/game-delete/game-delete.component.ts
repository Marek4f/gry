import { Usuwanie } from './../function/fun3';
import { AppComponent } from './../app.component';
import { Component, OnInit } from '@angular/core';
import {ReflectiveInjector} from '@angular/core';
import { FormControl, FormGroup, FormBuilder, AbstractControl, Validators } from '@angular/forms';
import { titles } from  './../app.component';
@Component({
  selector: 'app-game-delete',
  templateUrl: './game-delete.component.html',
  styleUrls: ['./game-delete.component.css']
})
export class GameDeleteComponent implements OnInit {
   titles: Array<any>;
  myForm2: FormGroup;

  tytul: AbstractControl;
  usuwacz: Usuwanie;
  constructor(fb: FormBuilder) {
    this.titles=titles;
    const injector: any = ReflectiveInjector.resolveAndCreate([Usuwanie]);
    this.usuwacz = injector.get(Usuwanie);
   
    this.myForm2 = fb.group({
      "tytul": ['', Validators.required]
    });
        this.tytul = this.myForm2.controls['tytul'];
   }

  ngOnInit() {
  }

remove(value: any) {

    this.titles = this.usuwacz.usun(value.tytul,this.titles);


  }

}
