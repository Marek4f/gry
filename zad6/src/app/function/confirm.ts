import { SearchComponent } from '../search/search.component';
import { Injectable } from '@angular/core';
import { Router, CanActivate, CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';


@Injectable()
export class ConfirmContactGuard implements CanDeactivate<SearchComponent> {

  constructor(private router: Router) { }

  canDeactivate(target: SearchComponent) {
     
        
    return window.confirm('Czy na pewno chcesz wyjść z wyszukiwarki?');
  }
}
