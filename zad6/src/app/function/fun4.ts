import { Injectable } from '@angular/core';

@Injectable()
export class Edytowanie {

    constructor() { }

    edytuj(a: any, b: any, d: any,  c: any) {
        for(let i = 0; i < c.length; i++){
           if (a === c[i].tytul){
               c[i].tytul = b;
               c[i].gatunek = d;
           }
       }
        return c;
    }
}