
class Gra{
    constructor(json){
    this.tytul = json.tytul;
    this.gatunek = json.gatunek;
    this.ocena = json.ocena;
    this.studio = json.studio;
    }
    toTableRow(){
		return `<tr><td>
		${this.tytul}
		</td><td>
		${this.gatunek}
		</td><td>
        ${this.ocena}
		</td><td>
        ${this.studio}
		</tr>`;
	}
}
class ListaGier{
    constructor(){
    this.gry = [];
    let self = this;
    }
    dodajGre(json) {
		this.gry.push(new Gra(json));
	}

    toTable() {
		let table = '<table><tr><th>Tytul</th> <th>Gatunek</th> <th>Ocena</th> <th>Studio</th></tr>';
		for(let i=0; i<this.gry.length; i++) {
			table += this.gry[i].toTableRow();
		}
		table += '</table>';
		return table;
	}

  

}

function init(){

let oReq = new XMLHttpRequest();
oReq.onload = reqListener;
oReq.open("get", "./dane.json", true);
oReq.send();
let json;
function reqListener(e) {
   json = JSON.parse(this.responseText);
   listaGier = new ListaGier();
    for(let i=0;json.length>i;i++){
        listaGier.dodajGre(json[i]);
    }

    let context = document.getElementById('table');
	context.innerHTML = listaGier.toTable();
}

   // console.log(json);

}
