var Gra = (function() {
	function Gra(json) {
		this.tytul = json.tytul;
		this.gatunek = json.gatunek;
		this.ocena = json.ocena;
		this.studio = json.studio;
	}	

	Gra.prototype.toTableRow = function() {
		return '<tr><td>'
		+ self.tytul
		+ '</td><td>'
		+ self.gatunek
		+ '</td><td>'
        + self.ocena
		+ '</td><td>'
        + self.studio
		+ '</tr>';
	}
	return Gra;
})();

var ListaGier = (function () {
	function ListaGier() {
        this.gry = [];
	}

	ListaGier.prototype.dodajGre = function(json) {
		this.gry.push(new Gra(json));
	}

	ListaGier.prototype.toTable = function() {
		var self = this;
		self.table = '<table>';
		self.table += this.generateTableHeader();
		for(var i=0; i<this.gry.length; i++) {
			self.table += this.gry[i].toTableRow();
		}
		self.table += '</table>';
		return self.table;
	}

	ListaGier.prototype.generateTableHeader = function() {
		return '<tr><th>Tytul</th> <th>Gatunek</th> <th>Ocena</th> <th>Studio</th></tr>';
	}	
	return ListaGier;
})();

function init() {
	var req = new XMLHttpRequest();
	req.onload = reqListener;
	req.open("get", "./data.json", true);
	req.send();
	var json;

	function reqListener(e) {
   		json = JSON.parse(this.responseText);
   		var listaGier = new listaGier();
    	for(var i=0;json.length>i;i++) {
        	listaGier.dodajGre(json[i]);
        }

    	var context = document.getElementById('table');
		context.innerHTML = listOfFilmy.toTable();
	}
}
