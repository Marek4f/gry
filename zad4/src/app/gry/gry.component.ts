import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gry',
  templateUrl: './gry.component.html',
  styleUrls: ['./gry.component.css']
})
export class GryComponent implements OnInit {
  title: string[];
  category: string[];
  constructor() { 
    this.title = ['Wiedzmin 3', 'Mass Effect','The Walking Dead', 'BioShock', 'GTA V'];
    this.category = ['RPG', 'RPG', 'PRZYGODOWA', 'FPS', 'SANDBOX']
  }

  ngOnInit() {
  }
gameClicked(gameTitle: string){
  console.log(`Gatunek- ${this.category[this.title.indexOf(gameTitle)]}`)
}
}
