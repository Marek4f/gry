import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';

import { GryComponent } from './gry/gry.component';
import { GryItemComponent } from './gry-item/gry-item.component';

@NgModule({
  declarations: [
    AppComponent,

    GryComponent,
    GryItemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
