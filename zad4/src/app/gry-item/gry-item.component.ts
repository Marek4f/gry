import { Component, OnInit, Input,Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-gry-item',
  templateUrl: './gry-item.component.html',
  styleUrls: ['./gry-item.component.css']
})
export class GryItemComponent implements OnInit {
@Input()
name: string;
@Output()
clickEmitter: EventEmitter<string> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
  gameSelected() {
    console.log(`Szczegoly gry ${this.name}:`);
    this.clickEmitter.emit(this.name);
  }
}
